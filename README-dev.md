# __APP_NOME__

## Visão 
Desenvolver um sistema web para atenteder a necessidades do [...] de acordo com as metodologias especificadas pelo Instituto Chico Mendes de Biodiversidade (ICMBio).

## Objetivo do Sistema

Objetivo 1
Objetivo 2


## Instalação ambiente de desenvolvimento

### Pré-requisitos:
Ter instalado o docker e docker-compose na máquina.

Construção de ambiente de desenvolvimento alinhado às normas do PDCI do ICMBio.
#### Opção 1 - Utilizando os scripts de deploys

1) Subindo container com banco de dados no DEV

De permissão de execução no arquivo de deploy do DB:

     ```
     chmod +x deploy_dev__db_docker-compose.sh
     ```
     
Execute o script de deploy e siga as intruções.     
          ```
          ./deploy_dev__db_docker-compose.sh dev
          ```

2) Subindo suite da aplicação no DEV

De permissão de execução no arquivo de deploy da app:

     ```
     chmod +x deploy_dev__docker-compose.sh
 
     ```
     
Execute o script de deploy e siga as intruções.     
          ```
          ./deploy_dev__docker-compose.sh dev
          ```     

Neste ponto vc deve ter os containers rodando em sua máquina.

Importante configurar o seu arquivo `/etc/hosts` com as seguintes linhas (substituindo pelo seu IP)

```
192.168.1.140 sicae.__APP_NOME__.sisicmbio.icmbio.gov.br
192.168.1.140 dev.__APP_NOME__.sisicmbio.icmbio.gov.br
```

Pode acessar o http://dev.__APP_NOME__.sisicmbio.icmbio.gov.br para verificar se a aplicação com a última imagem publicada está rodando.

          
#### Opção 2 - Utilizando o jenkins DEV
1. Clonar o repositório https://gitlab.com/pdci/dev-dockerhost-co7-pdci 

2. Instalar o Jenkins utilizando o docker da máquina local, ao invés de uma VM (Vagrant):
   * Editar o arquivo `env.dev` no parâmetro `pdci_docker_host_dev=tcp://192.168.56.101:2375` substituindo por um IP de uma interface de rede de sua máquina.
   * Dar permissão de execução ao script `pdci_ubuntu_install_dev_suite_sem_vagant.sh`
     ```
     chmod +x pdci_ubuntu_install_dev_suite_sem_vagant.sh
     ```
   * Executar o script `./pdci_ubuntu_install_dev_suite_sem_vagant.sh`
3. Acessar o Jenkins no endereço informado no final da execução do script

4. Configurar o Job `dev-exemplo-pdci-033-criar-jobs-para-projeto` na seção `Bindings` configurar `Credentials` adicionando uma em branco (sem usuário e senha) e associando aos dois itens.
5. Executar o Job `dev-exemplo-pdci-033-criar-jobs-para-projeto`, isso irá criar os Jobs específicos do projeto __APP_NOME__.
6. Configurar o Job `pdci-app___APP_NOME__-051-deploy-bd-postgis-em-DEV-db-sem-app` na seção `Source Code Management` adicionando e associando suas credenciais do gitlab.com. Utilizar as mesmas credenciais na seção `Bindings` para configurar acesso ao registry.gitlab.com.
7. Executar o Job `pdci-app___APP_NOME__-051-deploy-bd-postgis-em-DEV-db-sem-app`. :hourglass: :coffee: :coffee: :coffee: Isso vai trazer o dump do banco com os schemas necessários para o desenvolvimento. Em torno de 1G de download, e depois mais um tempo para restaurar.
8. Configurar o Job `pdci-app___APP_NOME__-057-deploy-em-DEV` na seção `Source Code Management` associando suas credenciais do gitlab.com e nos `Bindings` com Username Variable igual a `__pdci_registry_gitlab_deploy_token_username`.
9. Executar o Job `pdci-app___APP_NOME__-057-deploy-em-DEV`. 

Neste ponto vc deve ter os containers rodando em sua máquina.

Importante configurar o seu arquivo `/etc/hosts` com as seguintes linhas (substituindo pelo seu IP)

```
192.168.1.140 sicae.__APP_NOME__.sisicmbio.icmbio.gov.br
192.168.1.140 dev.__APP_NOME__.sisicmbio.icmbio.gov.br
```

Pode acessar o http://dev.__APP_NOME__.sisicmbio.icmbio.gov.br para verificar se a aplicação com a última imagem publicada está rodando.

### Rodando com docker-compose para ter acesso às saídas do runserver e debug.

Pare o container do `app___APP_NOME__`:

* verifique o ID do container usando `docker ps | grep app___APP_NOME__`
* execute `docker stop ID_DO_CONTAINER`

Crie um arquivo baseado em `.env.docker.local` com o nome `.env`

```
cp .env.docker.local .env
```

Edite o arquivo, principalmente os parâmetros abaixo:

```
HOST_IP=192.168.1.140
```

Finalmente suba somente o container do __APP_NOME__ com compose:


```
docker-compose -p pdci_dev_suite_development -f dev__docker-compose.yml -f local__docker-compose.yml run __APP_NOME__
```












