DO
$do$
BEGIN
 IF NOT EXISTS (
     SELECT                       -- SELECT list can stay empty for this
     FROM   pg_catalog.pg_roles
     WHERE  rolname = 'usr___APP_NOME__') THEN

   create user usr___APP_NOME__ with password 'usr___APP_NOME__';
 END IF;

 IF NOT  EXISTS (
     SELECT -- SELECT list can stay empty for this
     FROM information_schema.tables  WHERE table_schema='__APP_NOME__'
 )
 THEN
  CREATE SCHEMA IF NOT EXISTS __APP_NOME__;

  grant select,insert,update on all tables in schema __APP_NOME__ to usr___APP_NOME__;
  grant usage on all sequences in schema __APP_NOME__ to usr___APP_NOME__;
  grant execute on all functions in schema __APP_NOME__ to usr___APP_NOME__;


 -- GRANT ALL ON all tables in SCHEMA __APP_NOME__,sica,sicae,corporativo,auditoria,taxonomia,salve TO usr_jenkins WITH GRANT OPTION; deverar ser comentado com o usuario postgres
 -- GRANT ALL ON all functions in SCHEMA __APP_NOME__,sica,sicae,corporativo,auditoria,taxonomia,salve TO usr_jenkins WITH GRANT OPTION; deverar ser comentado com o usuario postgres
 -- GRANT ALL ON all sequences in SCHEMA __APP_NOME__,sica,sicae,corporativo,auditoria,taxonomia,salve TO usr_jenkins WITH GRANT OPTION; deverar ser comentado com o usuario postgres
  
  grant select,insert,update on all tables in schema __APP_NOME__ to usr_jenkins;
  grant usage on all sequences in schema __APP_NOME__ to usr_jenkins;
  grant execute on all functions in schema __APP_NOME__ to usr_jenkins;

 END IF;

 IF  EXISTS (
     SELECT -- SELECT list can stay empty for this
     FROM information_schema.tables  WHERE table_schema='taxonomia'
 )
 THEN
  GRANT USAGE ON SCHEMA taxonomia TO usr___APP_NOME__;
  GRANT SELECT ON TABLE taxonomia.vw_nivel_taxonomico TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION taxonomia.search_taxon(CHARACTER VARYING, BIGINT) TO usr___APP_NOME__;
 end if;

 IF  EXISTS (
     SELECT -- SELECT list can stay empty for this
     FROM information_schema.tables  WHERE table_schema='sicae'
 )
 THEN
  GRANT USAGE ON SCHEMA sicae TO usr___APP_NOME__;
  GRANT SELECT ON TABLE sicae.vw_usuario_pessoa_fisica TO usr___APP_NOME__;
  GRANT SELECT ON TABLE sicae.vw_usuario_externo TO usr___APP_NOME__;
  GRANT SELECT ON TABLE sicae.vw_usuario TO usr___APP_NOME__; -- USUARIO INTERNO
  GRANT SELECT ON TABLE sicae.vw_montar_menu TO usr___APP_NOME__;
  GRANT SELECT ON TABLE sicae.vw_menu_hierarquico TO usr___APP_NOME__;
  GRANT SELECT ON TABLE sicae.vw_menu_hierarq_manter TO usr___APP_NOME__;
  GRANT SELECT ON TABLE sicae.vw_perfil TO usr___APP_NOME__;
  GRANT SELECT ON TABLE sicae.vw_usuario_perfil TO usr___APP_NOME__;
  GRANT SELECT ON TABLE sicae.vw_usuario_pessoa_fisica TO usr___APP_NOME__;
  GRANT SELECT ON TABLE sicae.vw_perfil_funcionalidade TO usr___APP_NOME__;
  GRANT SELECT ON TABLE sicae.vw_rota TO usr___APP_NOME__;
  GRANT SELECT ON TABLE sicae.vw_sistema TO usr___APP_NOME__;
  GRANT SELECT ON TABLE sicae.perfil_funcionalidade TO usr___APP_NOME__;
 end if;

 IF  EXISTS (
     SELECT -- SELECT list can stay empty for this
     FROM information_schema.tables  WHERE table_schema='canie'
 )
 THEN
  GRANT USAGE ON SCHEMA canie TO usr___APP_NOME__;
  GRANT SELECT ON TABLE canie.vw_caverna_localidade TO usr___APP_NOME__;
 end if;

 IF  EXISTS (
     SELECT -- SELECT list can stay empty for this
     FROM information_schema.tables  WHERE table_schema='corporativo'
 )
 THEN
  GRANT USAGE ON SCHEMA corporativo TO usr___APP_NOME__;
  GRANT SELECT ON TABLE corporativo.vw_pessoa_fisica TO usr___APP_NOME__;
  GRANT SELECT ON TABLE corporativo.vw_pessoa_juridica TO usr___APP_NOME__;
  GRANT SELECT ON TABLE corporativo.mv_geo_unidade_conservacao TO usr___APP_NOME__;
  GRANT SELECT ON TABLE corporativo.geo_unidades_conservacao TO usr___APP_NOME__;
  GRANT SELECT ON TABLE corporativo.vw_unidade_org_externa TO usr___APP_NOME__;
  GRANT SELECT ON TABLE corporativo.vw_unidade_org_interna TO usr___APP_NOME__;
  GRANT SELECT ON TABLE corporativo.vw_pessoa TO usr___APP_NOME__;
  GRANT SELECT ON TABLE corporativo.vw_pessoa_fisica TO usr___APP_NOME__;
 end if;

 IF  EXISTS (
     SELECT -- SELECT list can stay empty for this
     FROM information_schema.tables  WHERE table_schema='auditoria'
 )
 THEN
  GRANT USAGE ON SCHEMA auditoria TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION auditoria.coluna(CHARACTER VARYING, CHARACTER VARYING, CHARACTER VARYING) TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION auditoria.conteudo_coluna(text, xml) TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION auditoria.trilha_anterior(BIGINT) TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION auditoria.trilha_em_lote(CHARACTER VARYING, CHARACTER VARYING, CHARACTER VARYING, INTEGER, CHARACTER VARYING, CHARACTER VARYING, CHARACTER, text) TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION auditoria.trilha_em_lote(INTEGER, INTEGER, INTEGER, INTEGER, CHARACTER VARYING, CHARACTER VARYING, CHARACTER, text) TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION auditoria.trilha_insere(INTEGER, INTEGER, INTEGER, INTEGER, INTEGER, CHARACTER, BOOLEAN, xml) TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION auditoria.trilha_insere(INTEGER, INTEGER, INTEGER, INTEGER, CHARACTER, xml) TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION auditoria.trilha_insere(CHARACTER VARYING, CHARACTER VARYING, CHARACTER VARYING, INTEGER, INTEGER, CHARACTER, BOOLEAN, xml) TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION auditoria.trilha_insere(INTEGER, INTEGER, INTEGER, INTEGER, CHARACTER, BOOLEAN, xml) TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION auditoria.trilha_insere(CHARACTER VARYING, CHARACTER VARYING, CHARACTER VARYING, INTEGER, CHARACTER, xml) TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION auditoria.trilha_insere(CHARACTER VARYING, CHARACTER VARYING, CHARACTER VARYING, INTEGER, CHARACTER, BOOLEAN, xml) TO usr___APP_NOME__;
  GRANT EXECUTE ON FUNCTION auditoria.trilha_metadata(xml) TO usr___APP_NOME__;
 end if;

 IF  EXISTS (
     SELECT -- SELECT list can stay empty for this
     FROM information_schema.tables  WHERE table_schema='salve'
 )
 THEN
  GRANT USAGE ON SCHEMA salve TO usr___APP_NOME__;
  GRANT SELECT ON TABLE salve.vw_ficha TO usr___APP_NOME__;
 end if;

  GRANT USAGE ON SCHEMA __APP_NOME__ TO usr___APP_NOME__;
  grant select,insert,update on all tables in schema __APP_NOME__ to usr___APP_NOME__;
  grant usage on all sequences in schema __APP_NOME__ to usr___APP_NOME__;
  grant execute on all functions in schema __APP_NOME__ to usr___APP_NOME__;

END;
$do$;