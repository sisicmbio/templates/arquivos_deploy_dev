#!/usr/bin/env bash

#set -ex

ls -lah /var/dump/

docker -H ${__PDCI_DOCKER_HOST} login ${__PDCI_REGISTRY}  --username gitlab+deploy-token-53234 --password -csiwEoaaCWhV7Asd_uG

nome_arquivo=$(ls /var/dump/docker*.dump | head -1)
nome_arquivo_user=$(ls /var/dump/create_user_docker*.sql | head -1)

echo "nome_arquivo_user => ${nome_arquivo_user}"
echo "nome_arquivo => ${nome_arquivo}"
echo ""

if [ "${nome_arquivo_user}" == "" ]; then
echo
echo "O arquivo /var/dump/create_user_docker*.sql nao foi encontrado!"
echo
exit 1

fi

if [ "${nome_arquivo}" == "" ]; then

echo
echo "O arquivo /var/dump/docker*.dump nao foi encontrado!"
echo
exit 1

fi

echo ""
echo "Iniciando restauracao da imagem do dump de produção disponível numa imagem_file_dump-prd"

docker  -H ${__PDCI_DOCKER_HOST}  network create pdci_suite_${PDCI_APPLICATION_ENV}_network || true


echo "version: '2.2'
volumes:
  pdci_postgis_data_tmp_db_full_prd:
  pdci_postgis_data_tbs_index_ssd:
  pdci_postgis_data_tbs_geo:
networks:
  pdci_networks:
    external:
      name: pdci_suite_${PDCI_APPLICATION_ENV}_network
services:
  tmp_db_full_prd:
    image: registry.gitlab.com/pdci/postgis:latest
    container_name: db_base_full_latest
    networks:
    - pdci_networks
    environment:
    - POSTGRES_DB=docker
    - POSTGRES_USER=docker
    - POSTGRES_PASS=docker
    - ALLOW_IP_RANGE=0.0.0.0/0
    cpu_count: 8
    cpu_percent: 70
#    mem_limit: 7192m
    #mem_reservation: 512m
    volumes:
    - pdci_postgis_data_tmp_db_full_prd:/var/lib/postgresql/10/main
    - pdci_postgis_data_tbs_index_ssd:/index_ssd
    - pdci_postgis_data_tbs_geo:/geo
    ports:
    - 55432:5432" > docker-compose-restaura-bd-prd-local.yml

docker-compose -H ${__PDCI_DOCKER_HOST} -p pdci_suite_${PDCI_APPLICATION_ENV} -f docker-compose-restaura-bd-prd-local.yml pull

docker-compose -H ${__PDCI_DOCKER_HOST} -p pdci_suite_${PDCI_APPLICATION_ENV} -f docker-compose-restaura-bd-prd-local.yml rm -f -s -v

docker -H ${__PDCI_DOCKER_HOST}  volume rm pdci_suite_${PDCI_APPLICATION_ENV}_pdci_postgis_data_tmp_db_full_prd || true

docker -H ${__PDCI_DOCKER_HOST}  volume rm pdci_suite_${PDCI_APPLICATION_ENV}_pdci_postgis_data_tbs_index_ssd || true

docker -H ${__PDCI_DOCKER_HOST}  volume rm pdci_suite_${PDCI_APPLICATION_ENV}_pdci_postgis_data_tbs_geo || true

docker-compose -H ${__PDCI_DOCKER_HOST} -p pdci_suite_${PDCI_APPLICATION_ENV} -f docker-compose-restaura-bd-prd-local.yml up -d

docker-compose -H ${__PDCI_DOCKER_HOST} -p pdci_suite_${PDCI_APPLICATION_ENV} -f docker-compose-restaura-bd-prd-local.yml ps


#PDCI_BD_HOST=${PDCI_BD_HOST:-10.197.93.75}
#PDCI_BD_PORT=${PDCI_BD_PORT:-55432}

PDCI_BD_HOST=${PDCI_BD_HOST:-tmp_db_full_prd}
PDCI_BD_PORT=${PDCI_BD_PORT:-5432}
PDCI_BD_NAME=db_dev_cotec
PDCI_DB_USER=docker
PDCI_DB_PASSWORD=docker

#PDCI_BD_HOST=$(echo "${__PDCI_DOCKER_HOST}" | sed -e "s|tcp://||g" | sed -e "s|:2375||g")
#echo "PDCI_BD_HOST => ${PDCI_BD_HOST}"
#PDCI_BD_PORT=55432
#PDCI_BD_NAME=db_dev_cotec

echo ""
echo "Esperando conexao com o  base de dados ${PDCI_BD_NAME} em 180 segundos"
echo ""

sleep 180




declare -a arr_all_jobs=("usr_sicae"
"usr_libcorp"
"usr_geo"
"usr_taxonomia"
"usr_salve"
"usr_sisbio"
"usr_canie"
"usr_infoconv"
"usr_sarr"
"usr_compensacao"
"usr_sica"
"usr_siorg"
"usr_simac"
"usr_brigadista"
"usr_capacitacao"
"usr_sintax"
"usr_monitora"
"usr_lafsisbio")

echo ""
echo "Criando usuario da aplicacoes ${PDCI_BD_NAME}"
echo ""

for i in "${arr_all_jobs[@]}"
do
echo ""
echo "Usuario:: ${i}"
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -c "DO \$do$
    BEGIN

        IF NOT EXISTS (
                SELECT 1
                FROM   pg_catalog.pg_roles
                WHERE  rolname = '${i}')
        THEN

            CREATE ROLE ${i} LOGIN PASSWORD '${i}';
        ELSE
            SELECT \"Usuario ${i} ja existe.\";
        END IF;

    END
\$do$;";

done





echo ""
echo "Limpar base de dados ${PDCI_BD_NAME}"
echo ""



PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '${PDCI_BD_NAME}'";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -c "DROP DATABASE IF EXISTS ${PDCI_BD_NAME}" ;

echo ""
echo "Criar base de dados geo ${PDCI_BD_NAME} vazia"
echo ""

PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -c "CREATE DATABASE ${PDCI_BD_NAME}  WITH OWNER = postgres; ";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "DO \$do$ BEGIN IF NOT EXISTS( SELECT  FROM   pg_catalog.pg_roles  WHERE  rolname = 'usr_jenkins' ) THEN  create user usr_jenkins with password 'usr_jenkins';  END IF;  END; \$do$;";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS fuzzystrmatch CASCADE;";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS postgis  CASCADE;";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS postgis_tiger_geocoder  CASCADE;";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS postgis_topology  CASCADE;";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS pg_trgm  CASCADE;";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS tablefunc  CASCADE;";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS adminpack  CASCADE;";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "DROP TABLESPACE  IF EXISTS tbs_index_ssd;";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "CREATE TABLESPACE tbs_index_ssd LOCATION '/index_ssd';";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "DROP TABLESPACE  IF EXISTS tbs_geo;";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "CREATE TABLESPACE tbs_geo LOCATION '/geo';";

PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} < contrib/pg-10/postgis-2.4/legacy.sql

echo ""
echo "Fim da criacao da base de dados geo ${PDCI_BD_NAME} vazia"
echo ""


PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} < ${nome_arquivo_user}

PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} < ${nome_arquivo}

echo "================================================================================================"
echo ""
echo "     Todos os usuários externos e internos terão a senha de login alterada para 'qaz123' ."
echo ""
echo "================================================================================================"

PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "UPDATE sicae.usuario_externo SET tx_senha=md5('qaz123');";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "UPDATE sicae.usuario SET tx_senha=md5('qaz123');";

echo "================================================================================================"
echo ""
echo "     Colocar as urls dos sistemas para dev"
echo ""
echo "================================================================================================"

PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "SELECT tx_url, REPLACE(tx_url, 'dsv.', '' ) FROM sicae.sistema where tx_url ilike '%//dsv.%';";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "UPDATE sicae.sistema SET tx_url=REPLACE(tx_url, 'dsv.', '' )  WHERE tx_url ilike '%//dsv.%';";

PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "SELECT tx_url, REPLACE(tx_url, 'tcti.', '' ) FROM sicae.sistema  where tx_url ilike '%//tcti.%';";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "UPDATE sicae.sistema SET tx_url=REPLACE(tx_url, 'tcti.', '' )  WHERE tx_url ilike '%//tcti.%';";

PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "SELECT tx_url, REPLACE(tx_url, 'dev.', '' ) FROM sicae.sistema  where tx_url ilike '%//dev.%';";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "UPDATE sicae.sistema SET tx_url=REPLACE(tx_url, 'dev.', '' )  WHERE tx_url ilike '%//dev.%';";

PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "SELECT tx_url, REPLACE(tx_url, 'hmg.', '' ) FROM sicae.sistema	where tx_url ilike '%//hmg.%';";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "UPDATE sicae.sistema SET tx_url= REPLACE(tx_url, 'hmg.', '' ) WHERE tx_url ilike '%//hmg.%';";

PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "SELECT tx_url, REPLACE(tx_url, 'https://', 'http://dev.' ) FROM sicae.sistema WHERE tx_url ilike 'https://%' AND  tx_url not ilike '%//dev.%';";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "UPDATE sicae.sistema SET tx_url= REPLACE(tx_url, 'https://', 'http://dev.' )  WHERE tx_url ilike 'https://%' AND  tx_url not ilike '%//dev.%';";

PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "SELECT tx_url, REPLACE(tx_url, 'http://', 'http://dev.' ) FROM sicae.sistema WHERE tx_url ilike 'http://%' AND  tx_url not ilike '%//dev.%';";
PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "UPDATE sicae.sistema SET tx_url= REPLACE(tx_url, 'http://', 'http://dev.' )  WHERE tx_url ilike 'http://%' AND  tx_url not ilike '%//dev.%';";

PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "SELECT tx_url FROM sicae.sistema ORDER BY tx_url ;";

echo "================================================================================================"
echo ""
echo "    LIMPANDO BASE TABELAS GRANDES DE DADOS"
echo ""
echo "================================================================================================"



PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE auditoria.trilha_auditoria CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ocorrencia_especie CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE corporativo.remanescentes_cerrado CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE corporativo.remanescentes_cerrado CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE corporativo.remanescentes_mata_atlantica CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE corporativo.remanescentes_caatinga CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.cprm_litologia CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.imovel_certificado CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.estados_cursos_dagua CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hp_curva_nivel CASCADE;";
#
#
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.acadebio CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.amazonia_legal CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ana_lagos_lagoas_zonas_umidas CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.aneel_bioenergia CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.aneel_cent_gerad_eolioeletricas CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.aneel_cent_gerad_hidreletricas CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.aneel_eolioeletricas CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.aneel_fotovoltaica CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.aneel_peq_cent_gerad_hidreletricas CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.aneel_usinas_hidreletricas CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.aneel_usinas_termeletricas CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.aneel_usinas_termonucleares CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.anp_bloco_exploracao CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.anp_campo_producao CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.area_edificada CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.area_priori_biod2007 CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.assentamentos CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_ag_barragem CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_ag_conduto_tubulacao CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_ag_edificacao CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_ag_garimpo CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_ag_mina CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_ag_posto_indigena CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_ag_salina CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_ag_usina CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hd_grupo_rochas CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hd_ilha CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hd_queda_dagua_linha CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hd_recife_area CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hd_recife_ponto CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hd_sumidouro CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hp_area_duna CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hp_banco CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hp_curva_batimetrica CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hp_curva_nivel CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hp_elemento_fisiografico_natural CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hp_linha_cumeada CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hp_ponto_cotado_altimetria CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hp_ponto_cotado_profundidade CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hp_praia CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_hp_relevo_marinho CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_lc_area_edificada CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_lm_area_historica CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_lm_area_militar CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_lm_area_preservacao_permanente CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_lm_folha_scn_cin CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_lm_marco_limite CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_lm_monumento CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_lm_politico_exterior CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_lm_terra_indigena_area CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_lm_terra_indigena_ponto CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_pr_estacao_meteorologica CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_pr_ponto_extremo CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_pr_ponto_referencia CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_st_hidrovia_area CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_st_hidrovia_linha CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.bcim_st_travessia_linha CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.centros_espec_icmbio CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.certificadas_sigef CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.cgesp_fauna CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.coapro_reservatorio CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.comag_samge_alvos CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.comag_samge_usos CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.coord_reg_pol CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.cprm_litologia CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.dmif_aai CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.dmif_aai_total_unidade CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.dmif_areas_embargadas CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.dmif_autos_infracao CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.dmif_corte_seletivo CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.dmif_deter CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.dmif_focos CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.dmif_preps CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.dmif_proae CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.dmif_prodes CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.dnpm CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.estacao_ana CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.estados_cursos_dagua CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.estados_declividade CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.estados_geomorfologia CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.estados_hipsometria CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.estados_pedologia CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.estados_trecho_energia CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.estados_vegetacao CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.funai_terra_indigena CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.geo_camada CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.geo_mapeia_intersect CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.geologia CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.glebas_federais CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.gt_pk_metadata CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ibge_aeroporto_internacional CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ibge_localidades CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ibge_rodovias CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ibge_veg_brasil2002 CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.imovel_certificado CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.infra_atrativos_linhas CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.infra_atrativos_pontos CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.mma_car CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.mma_uc_nao_federais CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.mosaico CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.mt_eixo_dutoviario CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.mt_eixo_ferroviario CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.mt_estacoes_ferrovias CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.mt_hidrovias CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.mt_linha_transmissao CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.mt_portos_terminais CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.mt_usinas_alcool CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.mt_usinas_gas CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ocorrencia_especie CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ocorrencias_erro_geom CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ottobacias_nivel1 CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ottobacias_nivel2 CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ottobacias_nivel3 CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ottobacias_nivel4 CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ottobacias_nivel5 CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ottobacias_nivel6 CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.parcelas_terra_legal CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.quilombolas_brasil CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.solos CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.sub_bacias CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.terra_indigena CASCADE;";
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "TRUNCATE geo.ti_estudo CASCADE;";




echo "================================================================================================"
echo ""
echo "    Processo de higenizacao dos dados"
echo ""
echo "================================================================================================"
#PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -c

#for entry in sql/*
#do

#  if [ -f "$entry" ]; then
  #PGPASSWORD=${PDCI_DB_PASSWORD} psql -U ${PDCI_DB_USER} -w -h ${PDCI_BD_HOST} -p ${PDCI_BD_PORT} -c

#  else
#	echo "DIRETORIO"
#  fi
#echo $vartest
#done
